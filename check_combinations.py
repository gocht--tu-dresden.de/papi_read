
import sys
import json
import itertools
import subprocess


if len(sys.argv) < 2:
    print("error, arguments!")
    sys.exit(-1)

data = {}
with open(sys.argv[1],"r") as f:
    data = json.load(f)

broken_pairs = {}  
if len(sys.argv) == 3:
    with open(sys.argv[2],"r") as f:
        broken_pairs = json.load(f)
    
found = False
for key,counters in data.items():
    for L in range(0, len(counters)+1):
        for cs in itertools.permutations(counters,L):
            arg = key
            for c in cs:
                arg = arg + c
            out = subprocess.run(["./build/test_papi_read", "-c", arg], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if "PAPI_ERR" in str(out.stdout):
                tmp = [key]
                for c in cs:
                    tmp.append(c)
                    
                for v in itertools.permutations(tmp):
                    if v[0] not in broken_pairs:
                        broken_pairs[v[0]] = []
                    for v_ in v[1:]:
                        broken_pairs[v[0]].append(v_)
                    
                found = True
                break

        if found:
            break
    if found:
        found = False
        continue

if len(sys.argv) == 3:
    file = sys.argv[2]
else:
    file = "./broken_cobinations_dict.json"
    
with open(file,"w") as f:
    json.dump(broken_pairs,f,indent= 4, sort_keys=True)
