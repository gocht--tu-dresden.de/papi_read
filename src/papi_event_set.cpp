/*
 * papi_event_set.cpp
 *
 *  Created on: 27.01.2017
 *      Author: gocht
 */

#include <condition_variable>
#include <fstream>
#include <system_error>

#include <chrono>
#include <iostream>

#include <papi_event_set.h>
#include <string.h>

extern "C" {
#include <sched.h>
}

namespace papi
{
papi_event_set::papi_event_set(int responsible_core_, int cpus_)
    : responsible_core(responsible_core_), availabl_cpus(cpus_)
{
    cpu_set = CPU_ALLOC(cpus_);
    if (cpu_set == NULL)
    {
        throw papi_error("error allocatong cpu_set_t: CPU_ALLOC");
    }

    cpusetsize = CPU_ALLOC_SIZE(cpus_);
    CPU_ZERO_S(cpusetsize, cpu_set);

    for (int cpu = 0; cpu < cpus_; ++cpu)
    {
        int core_id;
        auto filename = std::string("/sys/devices/system/cpu/cpu") + std::to_string(cpu) +
                        std::string("/topology/core_id");
        std::ifstream sysfile(filename, std::ios_base::in);
        sysfile >> core_id;
        if (core_id == responsible_core)
        {
            responsible_cpu = cpu;
            break;
        }
    }
    CPU_SET_S(responsible_cpu, cpusetsize, cpu_set);
}

/* this might not be a very deadlock free solution .... moving of threads is a misrable idea.
 * however, this should work if the object is just moved once.
 */
papi_event_set::papi_event_set(papi_event_set &&other) noexcept
{
    names = other.names;
    responsible_core = other.responsible_core;
    responsible_cpu = other.responsible_cpu;
    availabl_cpus = other.availabl_cpus;

    cpu_set = CPU_ALLOC(other.availabl_cpus);
    if (cpu_set == NULL)
    {
        std::cerr << "error allocating cpu_set_t: CPU_ALLOC" << std::endl;
    }
    memcpy(cpu_set, other.cpu_set, CPU_ALLOC_SIZE(other.availabl_cpus));

    cpusetsize = other.cpusetsize;

    executing_thread = std::move(other.executing_thread);

    worker_action = other.worker_action;
    worker_done = other.worker_done;
    action = other.action;

    values = other.values;
}

papi_event_set::~papi_event_set()
{
    if (executing_thread.joinable())
    {
        action = actions::stop;
        worker_action = true;
        cv.notify_all();
        try
        {
            executing_thread.join();
        }
        catch (std::system_error &e)
        {
            std::cerr << e.what() << std::endl;
        }
    }

    CPU_FREE(cpu_set);
}

void papi_event_set::set_events(std::vector<std::string> names)
{
    if (!started)
    {
        executing_thread = std::thread(&papi_event_set::run, this);
        started = true;
    }

    this->names = names;

    action = actions::reset_counter;
    worker_action = true;
    cv.notify_all();
    {
        std::unique_lock<std::mutex> lock(m);
        cv.wait(lock, [this] { return worker_done; });
        worker_done = false;
        if (worker_error)
        {
            auto tmp_err = worker_error_msg;
            worker_error_msg = "";
            worker_error = false;
            throw papi_error(tmp_err);
        }
    }
}

void papi_event_set::run()
{
    sched_setaffinity(0, cpusetsize, cpu_set);

    int ret;
    int i = 0;
    do
    {
        i++;
        event_set = PAPI_NULL;
        ret = PAPI_create_eventset(&event_set);
    } while ((ret != PAPI_OK) && (i < 20));

    if (ret != PAPI_OK)
    {
        throw papi_error(std::string("Error during PAPI_create_eventset (tried 20 times): ") +
                         std::string(PAPI_strerror(ret)));
    }

    bool running = true;
    while (running)
    {
        std::unique_lock<std::mutex> lock(m);
        cv.wait(lock, [this] { return worker_action; });
        switch (action)
        {
            case actions::stop:
            {
                running = false;
                break;
            }
            case actions::update_data:
            {
                PAPI_read(event_set, values.data());
                break;
            }
            case actions::reset_counter:
            {
                /* ok clean up, just in case*/
                std::vector<long long> unused_data(values.size());
                PAPI_stop(event_set, unused_data.data());
                PAPI_cleanup_eventset(event_set);

                values.clear();
                values.resize(names.size(), 0);

                std::string already_added = "";
                for (auto &name : names)
                {
                    /* this is quite bad. I would prefer to copy the c_str to a new char array.
                     * However, papi and cstring don't like each other therfore I remove the const.
                     */
                    auto tmp_name = const_cast<char *>(name.c_str());
                    int ret;
                    int i = 0;
                    do
                    {
                        i++;
                        ret = PAPI_add_named_event(event_set, tmp_name);
                    } while ((ret != PAPI_OK) && (i < 20));

                    if (ret != PAPI_OK)
                    {
                        std::string err = std::string(
                                              "PAPI_ERR:: Error during PAPI_add_named_event (tried "
                                              "20 times) for: ") +
                                          name + std::string(" error:") +
                                          std::string(PAPI_strerror(ret)) +
                                          std::string("\n\talready added: \"") + already_added +
                                          std::string("\"");
                        trouble_sets.push_back(std::make_pair(already_added, tmp_name));
                        worker_error = true;
                        worker_error_msg = err;
                        break;
                    }
                    else
                    {
                        already_added += name;
                        already_added += ";";
                    }
                }
                if (!worker_error)
                {
                    int ret = PAPI_start(event_set);
                    if (ret != PAPI_OK)
                    {
                        worker_error_msg = std::string("Error during PAPI_start on cpu:  ") +
                                           std::to_string(responsible_cpu) + std::string(" : ") +
                                           std::string(PAPI_strerror(ret));
                        worker_error = true;
                    }
                }
                break;
            }
            case actions::none:
            default:
                break;
        }
        worker_action = false;
        worker_done = true;
        cv.notify_all();
    }
}

std::map<std::string, long long> papi_event_set::get_data()
{
    if (executing_thread.joinable())
    {
        action = actions::update_data;
        worker_action = true;
        cv.notify_all();
        {
            std::unique_lock<std::mutex> lock(m);
            cv.wait(lock, [this] { return worker_done; });
            worker_done = false;
        }

        std::map<std::string, long long> result;
        for (uint i = 0; i < names.size(); ++i)
        {
            result[names[i]] = values[i];
        }
        return result;
    }
    else
    {
        return std::map<std::string, long long>();
    }
}

std::vector<std::pair<std::string, std::string>> papi_event_set::get_counters_with_errors()
{
    return trouble_sets;
}
}  // namespace papi
