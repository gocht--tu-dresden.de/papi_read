/*
 * scorep_plugin_papi.h
 *
 *  Created on: 28.09.2016
 *      Author: andreas
 */

#include <fcntl.h>
#include <papi.h>
#include <sched.h>
#include <unistd.h>

#include <read_papi.h>

namespace papi
{

/* Initializes the papi lib, and c++ interface
 *
 * @param cpus_ amount of cpus to use for mesurment. If -1 is passed, all available CPU's are used
 *
 */
read_papi::read_papi(int cpus_ = -1)
{

    auto ret = PAPI_library_init(PAPI_VER_CURRENT);
    if (ret != PAPI_VER_CURRENT)
    {
        throw papi_error("Wrong papi version. Build against: " + std::to_string(PAPI_VER_CURRENT) +
                         "used with: " + std::to_string(ret));
    }
    PAPI_thread_init((long unsigned int (*)()) std::this_thread::get_id);

    if (cpus_ == -1)
    {
        cpus = sysconf(_SC_NPROCESSORS_CONF);
    }
    else
    {
        cpus = cpus_;
    }

    auto tmp_ht = is_ht_enabled();
    switch (tmp_ht)
    {
    case 0:
        ht_enabled = false;
        break;
    case 1:
        ht_enabled = true;
        break;
    default:
        throw papi_error("could not determine if hyperthreading is enabled");
        break;
    }

    int step = 1;
    if (ht_enabled)
    {
        step = 2;
    }

    for (int cpu = 0; cpu < cpus; cpu += step)
    {
        papi_event_sets.push_back(papi_event_set(cpu / step, cpus));
    }
}

/* This function is used to set counters to read
 *
 * @param event_names vector with papi events to set
 */
void read_papi::set_counter(std::vector<std::string> event_names)
{
    for (auto &event_set : papi_event_sets)
    {
        event_set.set_events(event_names);
    }
}
/* This function is used to read the set counter
 *
 * @return the returned vector holds the counters for each processor. The map in the vector consits
 * of the counter name
 * and the value
 *
 */
std::vector<std::map<std::string, long long>> read_papi::read_counter()
{
    std::vector<std::map<std::string, long long>> result;
    for (auto &event_set : papi_event_sets)
    {
        result.push_back(event_set.get_data());
    }
    return result;
}

/* This function returns all papi sets that causes some trouble, while adding a counter.
 *
 * @return
 * 	std::vector<				--> each processor that collects papi events
 * 		std::vector<			--> all sets at one processor that causes an error
 * 			std::pair<			--> the event set wit errors
 * 				std::string,	--> the error occured while these events where loaded
 * 				std::string		--> and this was supposed to be added.
 * 			>
 * 		>
 * 	>
 *
 *
 */
std::vector<std::vector<std::pair<std::string, std::string>>> read_papi::get_counters_with_errors()
{
    std::vector<std::vector<std::pair<std::string, std::string>>> retrun_value;

    for (auto &papi_event_set : papi_event_sets)
    {
        retrun_value.push_back(papi_event_set.get_counters_with_errors());
    }
    return retrun_value;
}

/** Check for HT
 * hyperthreading is disabled if if the thread_siblings_list is only 2 bytes, 0 and EOF
 * */
int is_ht_enabled(void)
{
    int fd = open("/sys/devices/system/cpu/cpu0/topology/thread_siblings_list", O_RDONLY);
    char buf[64] = {0};
    if (fd < 0)
    {
        return -1;
    }
    ssize_t count = read(fd, buf, 64);
    switch (count)
    {
    case 0:
        return -1;
    case 2:
        return 0;
    default:
        return 1;
    }
}
}
