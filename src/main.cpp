/*
 * main.cpp
 *
 *  Created on: 11.01.2017
 *      Author: gocht
 */

#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include <read_papi.h>

int main(int argc, char *argv[])
{
    papi::read_papi papi(-1);

    int opt;
    std::string counters;

    while ((opt = getopt(argc, argv, "c:")) != -1)
    {
        switch (opt)
        {
        case 'c':
            counters = std::string(optarg);
            break;
        default: /* '?' */
            std::cerr << "Usage: " << argv[0] << " [-c counters]" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::cout << "Hello world" << std::endl;
    std::istringstream avail_counter(counters);

    std::vector<std::string> counters_to_evaluate;
    for (std::string line; std::getline(avail_counter, line, ',');)
    {
        counters_to_evaluate.push_back(line);
    }

    papi.set_counter(counters_to_evaluate);

    for (int i = 0; i < 10; i++)
    {
        auto t1 = std::chrono::system_clock::now();
        auto result = papi.read_counter();
        auto t2 = std::chrono::system_clock::now();

        auto dt = t2 - t1;
        std::cout << "measurment took: "
                  << std::chrono::duration_cast<std::chrono::microseconds>(dt).count() << "us"
                  << std::endl;
    }
}
