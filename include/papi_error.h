/*
 * error.h
 *
 *  Created on: 27.01.2017
 *      Author: gocht
 */

#ifndef INCLUDE_PAPI_ERROR_H_
#define INCLUDE_PAPI_ERROR_H_

namespace papi
{
class papi_error : public std::exception
{
public:
    papi_error(const std::string &what_arg)
    {
        message = what_arg;
    }

    void append_event_name(std::string event_name)
    {
        message += " ";
        message += event_name;
    }

    const char *what() const noexcept override
    {
        return message.c_str();
    }

private:
    std::string message;
};

}  // namespace papi

#endif /* INCLUDE_PAPI_ERROR_H_ */
