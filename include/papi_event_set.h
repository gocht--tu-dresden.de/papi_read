/*
 * papi_event_set.h
 *
 *  Created on: 27.01.2017
 *      Author: gocht
 */

#ifndef INCLUDE_PAPI_EVENT_SET_H_
#define INCLUDE_PAPI_EVENT_SET_H_

#include <atomic>
#include <condition_variable>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <papi_error.h>

extern "C" {
#include <papi.h>
}

namespace papi
{
enum actions
{
    update_data,
    stop,
    reset_counter,
    none
};

struct papi_event_set
{
    // We need some kind of constructor
    papi_event_set(int responsible_core_, int cpus_);
    papi_event_set(papi_event_set &&other) noexcept;

    papi_event_set(const papi_event_set &) = delete;
    papi_event_set &operator=(const papi_event_set &) = delete;

    ~papi_event_set();

    void set_events(std::vector<std::string> names);
    std::map<std::string, long long> get_data();

    std::vector<std::pair<std::string, std::string>> get_counters_with_errors();

private:
    std::vector<std::string> names;
    int responsible_core;
    int responsible_cpu;
    int availabl_cpus = 0;

    bool started = false;

    cpu_set_t *cpu_set = NULL;
    size_t cpusetsize = 0;

    std::thread executing_thread;
    std::mutex m;
    std::condition_variable cv;

    bool worker_action = false;
    bool worker_done = false;
    bool worker_error = false;
    std::string worker_error_msg = "";
    actions action = none;

    int event_set = PAPI_NULL;

    std::vector<long long> values;

    std::vector<std::pair<std::string, std::string>> trouble_sets;

    void run();
};
}

#endif /* INCLUDE_PAPI_EVENT_SET_H_ */
