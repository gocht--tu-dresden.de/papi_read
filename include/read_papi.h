#ifndef INCLUDE_READ_PAPI_H_
#define INCLUDE_READ_PAPI_H_

#include <chrono>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <papi.h>
#include <vector>

#include <papi_error.h>
#include <papi_event_set.h>

namespace papi
{

using values = std::vector<std::map<std::string, long long>>;

class read_papi
{
public:
    read_papi(int cpus_);

    void set_counter(std::vector<std::string> event_names);
    std::vector<std::map<std::string, long long>> read_counter();
    std::vector<std::vector<std::pair<std::string, std::string>>> get_counters_with_errors();

private:
    int cpus;
    bool ht_enabled;
    std::vector<papi_event_set> papi_event_sets;
};
int is_ht_enabled(void);
} // namespace papi
#endif /* INCLUDE_READ_PAPI_H_ */
